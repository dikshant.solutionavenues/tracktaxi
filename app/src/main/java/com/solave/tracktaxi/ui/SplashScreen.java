package com.solave.tracktaxi.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.solave.tracktaxi.R;
import com.solave.tracktaxi.service.GeoService;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends BaseActivity {

    private int splashHoldTime = 0;

    private final int AUTO_HIDE_DELAY_MILLIS = 3 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        init();
    }


    private void changeLayoutHeightWidth(View layout) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
        params.height = (int) (deviceWidth * 0.40f);
        params.height = (int) (deviceWidth * 0.40f * 1.04f);
        params.topMargin = (int) (deviceHeight * 0.08f);
        layout.setLayoutParams(params);
    }

    void init() {
        // The time of SplashScreen how longer it visible, this is in mili-second
        splashHoldTime = getResources().getInteger(R.integer.splash_time) * 1000;

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }


    }

    private void showAlertDialogBox() {


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(SplashScreen.this);

        alertDialog.setTitle("GPS settings");

        alertDialog.setMessage("GPS is not enabled. Do you want to turn on the gps");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 1);

            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        alertDialog.show();
    }


    @Override
    protected void onStart() {
        super.onStart();
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            startService(new Intent(SplashScreen.this, GeoService.class));

            // Timer which active for Defined Splash Time
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    proceed();
                }
            }, splashHoldTime);
            super.onStart();

        } else {
            showAlertDialogBox();
            Toast.makeText(this, "GPS is not Enabled in your device", Toast.LENGTH_SHORT).show();
        }
        super.onStart();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            proceed();
        }
        return super.onTouchEvent(event);
    }

    private void proceed() {
        if (this.isFinishing()) {
            return;
        }
        Intent i = new Intent(SplashScreen.this, Dashboard.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForward(SplashScreen.this, i, true);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        startActivityBackward(SplashScreen.this, null, true);
    }
}
