package com.solave.tracktaxi.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.solave.tracktaxi.AppController;
import com.solave.tracktaxi.R;
import com.solave.tracktaxi.preference.TTPreference;
import com.solave.tracktaxi.service.GeoService;

/**
 * Created by DELL  374 on 12/27/2016.
 */

public class Dashboard extends BaseActivity {

    private TextView taxiNumberTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        setContext(this);
        taxiNumberTV = (TextView) findViewById(R.id.taxiNumberTV);
        if (!TextUtils.isEmpty(TTPreference.getInstance().getStringValue(TTPreference.TAXI_NUMBER))) {
            taxiNumberTV.setText(TTPreference.getInstance().getStringValue(TTPreference.TAXI_NUMBER));
        }
        final String preField = taxiNumberTV.getText().toString().trim();

        if (TextUtils.isEmpty(TTPreference.getInstance().getStringValue(TTPreference.TAXI_NUMBER)))
            new MaterialDialog.Builder(context)
                    .title("TAXI Number")
                    .content("Enter your TAXI number for Tracking.")
                    .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS)
                    .input(null, preField, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(MaterialDialog dialog, CharSequence input) {
                            // Do something
                            settingUpLocationSystem();
                            taxiNumberTV.setText(input);
                            TTPreference.getInstance().saveStringValue(TTPreference.TAXI_NUMBER, input.toString());
                            dialog.dismiss();
                        }
                    })
                    .positiveText("SUBMIT")
                    .cancelable(false)
                    .show();
        else
            AppController.getInstance().updateLocation();

        taxiNumberTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(context)
                        .title("TAXI Number")
                        .content("Enter your TAXI number for Tracking.")
                        .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS)
                        .input(null, preField, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                                settingUpLocationSystem();
                                taxiNumberTV.setText(input);
                                TTPreference.getInstance().saveStringValue(TTPreference.TAXI_NUMBER, input.toString());
                                dialog.dismiss();
                            }
                        })
                        .positiveText("SUBMIT")
                        .cancelable(false)
                        .show();
            }
        });
    }

    private void settingUpLocationSystem() {
        takeAndroidMPermission();

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            startService(new Intent(Dashboard.this, GeoService.class));
        } else {
            showAlertDialogBox();
            Toast.makeText(this, "GPS is not Enabled in your device", Toast.LENGTH_SHORT).show();
        }
    }

    private void showAlertDialogBox() {


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Dashboard.this);

        alertDialog.setTitle("GPS settings");

        alertDialog.setMessage("GPS is not enabled. Do you want to turn on the gps");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 1);

            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        alertDialog.show();
    }
}
