package com.solave.tracktaxi.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidquery.AQuery;
import com.solave.tracktaxi.AppController;
import com.solave.tracktaxi.R;
import com.solave.tracktaxi.utils.AppUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL  374 on 12/27/2016.
 */

public class BaseActivity extends AppCompatActivity {

    public int deviceHeight = 0;
    public int deviceWidth = 0;

    public AQuery aq;

    public Context context;

    public Context setContext(Context context) {
        this.context = context;
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        deviceHeight = displayMetrics.heightPixels;
        deviceWidth = displayMetrics.widthPixels;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance().setActivity(this);
    }

    public void startActivityForward(Activity activity, Intent i, boolean isFinish) {
        AppUtil.startActivityForward(activity, i, isFinish);
    }

    public void startActivityBackward(Activity activity, Intent i, boolean isFinish) {
        AppUtil.startActivityBackward(activity, i, isFinish);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppUtil.hideSoftKeypad(BaseActivity.this);
        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
    }

    public void showSimpleDialog(Context mContext, String message) {
        new MaterialDialog.Builder(mContext)
                .content(message)
                .positiveText(mContext.getString(android.R.string.ok))
                .show();
    }

    private static final int REQUEST_EXTERNAL_STORAGE = 111;

    public void takeAndroidMPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int hasInternetPermission = checkSelfPermission(Manifest.permission.INTERNET);
            int hasLocationPermission = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
            int hasLocationSecondaryPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);

            List<String> permissions = new ArrayList<String>();

            if (hasInternetPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.INTERNET);
            }
            if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (hasLocationSecondaryPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_EXTERNAL_STORAGE);
            }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasBootPermission = checkSelfPermission(Manifest.permission.RECEIVE_BOOT_COMPLETED);
            List<String> permissions = new ArrayList<String>();
            if (hasBootPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.RECEIVE_BOOT_COMPLETED);
            }
            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_EXTERNAL_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        System.out.println("Permissions --> " + "Permission Granted: " + permissions[i]);


                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        System.out.println("Permissions --> " + "Permission Denied: " + permissions[i]);
                        Toast.makeText(BaseActivity.this, "Permission Denied of " + permissions[i], Toast.LENGTH_LONG).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}
