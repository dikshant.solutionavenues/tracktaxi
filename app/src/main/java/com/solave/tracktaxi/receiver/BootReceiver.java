package com.solave.tracktaxi.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

import com.solave.tracktaxi.service.GeoService;
import com.solave.tracktaxi.utils.AppUtil;


/**
 * Created by DELL  374 on 9/9/2016.
 */

public class BootReceiver extends BroadcastReceiver {

    private PowerManager.WakeLock _wakeLock;

    @Override
    public void onReceive(Context context, Intent intent) {

        AppUtil.Log("Boot Receiver Invoked");

        wakelockAcquire(context.getApplicationContext());
        context.startService(new Intent(context, GeoService.class));
    }

    public void wakelockAcquire(Context context) {
        if (_wakeLock != null)
            _wakeLock.release();

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        _wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WakeLock");
        _wakeLock.acquire();
    }
}
