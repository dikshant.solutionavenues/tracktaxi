package com.solave.tracktaxi.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.os.Environment;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.solave.tracktaxi.R;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by DELL  374 on 4/27/2016.
 */
public class AppUtil {

    public static void Log(String message) {
        Log.d("CheckMyRank", message);
        System.out.println(message);
    }

    public static boolean isEmpty(String message) {

        if (message == null) {
            return true;
        }
        if (message.equals("null")) {
            return true;
        }
        if (TextUtils.isEmpty(message)) {
            return true;
        } else
            return false;

    }

    /**
     * To hide the soft key pad if open
     */
    public static void hideSoftKeypad(Context context) {
        Activity activity = (Activity) context;
        InputMethodManager imm = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getWindowToken(), 0);
        }
    }

    public static Date getCurrentSystemDate() {
        return new Date();
    }

    public static String getCurrentDate() {
        return AppUtil.getFormattedDate(
                AppUtil.getCurrentSystemDate(), AppUtil.SimpleDateFormat_dd_MM_yyyy());
    }

    public static String getCurrentDateWithTime() {
        return AppUtil.getFormattedDate(
                AppUtil.getCurrentSystemDate(), AppUtil.SimpleDateFormat_dd_MM_yyyy_with_time());
    }

    public static String getFormattedDate(Date date, SimpleDateFormat sdf) {
        return sdf.format(date);
    }

    public static Date getDateIn_dd_MM_yyyy(String date) throws ParseException {
        return SimpleDateFormat_dd_MM_yyyy().parse(date);
    }

    public static Date getDateIn_dd_MM_yyyy_hh_mm(String date) throws ParseException {
        return SimpleDateFormat_yyyy_MM_dd_hh_mm_ss().parse(date);
    }

    public static SimpleDateFormat SimpleDateFormat_yyyy_MM_dd_hh_mm_ss() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    }


   /* public static Date getDateIn_dd_MM_yyyy_hh_mm(String date) throws ParseException{
        return  SimpleDateFormat_yyyy_MM_dd_hh_mm_ss().parse(date);
    }*/

    /* public static SimpleDateFormat SimpleDateFormat_yyyy_MM_dd_hh_mm_ss(){
         return new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.US);
     }
 */
    public static SimpleDateFormat SimpleDateFormat_dd_MM_yyyy_with_time() {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
    }

    public static SimpleDateFormat SimpleDateFormat_dd_MM_yyyy() {
        return new SimpleDateFormat("MMM yyyy", Locale.US);
    }

    public static Date getDateInDesiredPattern(String date, String pattern) throws ParseException {
        return new SimpleDateFormat(pattern).parse(date);
    }

    /**
     * This method also assumes endDate >= startDate
     **/
    public static long daysBetween(Date startDate, Date endDate) {
        Calendar sDate = getDatePart(startDate);
        Calendar eDate = getDatePart(endDate);

        long daysBetween = 0;
        while (sDate.before(eDate)) {
            sDate.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    public static String saveWithSingleQuote(String commitment) {
        return commitment.replaceAll("'", "''");
    }

    public static String showWithSingleQuote(String commitment) {
        return !TextUtils.isEmpty(commitment) ? commitment.replaceAll("''", "'") : commitment;
    }

    private static Calendar getDatePart(Date date) {
        Calendar cal = Calendar.getInstance();       // get calendar instance
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        cal.set(Calendar.MINUTE, 0);                 // set minute in hour
        cal.set(Calendar.SECOND, 0);                 // set second in minute
        cal.set(Calendar.MILLISECOND, 0);            // set millisecond in second
        return cal;                                  // return the date part
    }

    public static int getExifOrientation(String filepath) {
        int degree = 0;
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(filepath);
        } catch (IOException ex) {
        }
        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
            if (orientation != -1) {
// We only recognise a subset of orientation tag values.
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree = ExifInterface.ORIENTATION_ROTATE_90;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree = ExifInterface.ORIENTATION_ROTATE_180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree = ExifInterface.ORIENTATION_ROTATE_270;
                        break;
                }

            }
        }
        return degree;
    }

    public static void startActivityForward(Activity activity, Intent i, boolean isFinish) {
        AppUtil.hideSoftKeypad(activity);
        activity.startActivity(i);
        activity.overridePendingTransition(
                R.anim.slide_in_left,
                R.anim.slide_out_left);
        if (isFinish)
            activity.finish();
    }

    public static void startActivityBackward(Activity activity, Intent i, boolean isFinish) {
        AppUtil.hideSoftKeypad(activity);
        activity.overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
        if (isFinish)
            activity.finish();
        if (i != null)
            activity.startActivity(i);
    }

    private static final String APP_TAG = "BookAppAudioRecorder";

    public static int logString(String message) {
        return Log.i(APP_TAG, message);
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static File getImagePath() {
        final String appDirectoryName = "BookApp/";
        File dir = new File(Environment.getExternalStorageDirectory() + "/" + appDirectoryName);
        File file = new File(dir, "ProfilePic" + getUniqueTimeValue() + ".png");
        if (!dir.exists()) {
            if (dir.mkdirs()) {
                if (file.exists()) {
                    file.delete();
                }
            }
        } else {
            if (file.exists()) {
                file.delete();
            }
        }
        return file;
    }

    public static String getUniqueTimeValue() {
        long currentDateTime = System.currentTimeMillis();
        Date currentDate = new Date(currentDateTime);
        SimpleDateFormat df = new SimpleDateFormat("dd:MM:yy:HH:mm:ss");
        return currentDateTime + "";
    }


    public static Bitmap rotate(int value, Bitmap bm) {

        if (bm == null) return null;
        Bitmap result = bm;

        if (value > 0) {

            Matrix matrix = getRotateMatrix(value);
            result = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);


            if (bm != result) {
                bm.recycle();
            }
        }

        return result;
    }

    private static Matrix getRotateMatrix(int ori) {

        Matrix matrix = new Matrix();
        switch (ori) {
            case 0:
                matrix.setRotate(90);
                break;
            case 2:
                matrix.setScale(-1, 1);
                break;
            case 3:
                matrix.setRotate(180);
                break;
            case 4:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case 5:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case 6:
                matrix.setRotate(90);
                break;
            case 7:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case 8:
                matrix.setRotate(-90);
                break;

        }

        return matrix;
    }

    private static ProgressDialog progressDialog;

    public static void showProgressDialog(final Context context, boolean cancelable, String message) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCanceledOnTouchOutside(cancelable);
        progressDialog.setCancelable(cancelable);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public static void showProgressDialog(final Context context, String message) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public static void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public static String setColorFullText(String colorCode, String text){
        return "<font color='"+colorCode+"'>"+ text + "</font>";
    }
    public static void setRefineText(TextView tv, String text){
        tv.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
    }
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showToast(Context context, int messageId) {
        Toast.makeText(context, context.getString(messageId), Toast.LENGTH_LONG).show();
    }

    public static Typeface getRegularFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/DINPro-Regular.ttf");
    }

    public static Typeface getBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/DINPro-Bold.ttf");
    }

    public static Typeface getMediumFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/DINPro-Medium.ttf");
    }

    public static Typeface getLightFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/DINPro-Light.ttf");
    }

}
