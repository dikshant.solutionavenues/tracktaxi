package com.solave.tracktaxi.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by ranosys-sid on 15/5/15.
 */
public class TTPreference {
    private static String FILE_NAME = "TT_preference";
    private static int MODE_PRIVATE = 0;
    private SharedPreferences sharedPreferences;
    private static TTPreference pymPreference;

    public static String TAXI_NUMBER = "taxiNumber";
    public static String LATITUDE = "latitude";
    public static String LONGITUDE = "longitude";


    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public void setSharedPreferences(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    private String[] removeKey = {TAXI_NUMBER, LATITUDE, LONGITUDE};

    private TTPreference() {
    }

    public static void init(Context context) {
        if (pymPreference == null) {
            pymPreference = new TTPreference();
            pymPreference.sharedPreferences = context.getSharedPreferences(FILE_NAME, MODE_PRIVATE);
        }
    }

    public static TTPreference getInstance() {
        if (pymPreference == null) {
            Log.d(TTPreference.class.getName(), "TTPreference not initialize yet. Call TTPreference.init() to initialize it.");
        }
        return pymPreference;
    }

    public void saveStringValue(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public boolean getBooleanValue(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public void saveBooleanValue(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public String getStringValue(String key) {
        return sharedPreferences.getString(key, null);
    }

    public void clearPreference() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        for (String key : removeKey) {
            editor.remove(key);
        }
        editor.commit();
    }

    public void clearPreference(String key) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }
}
