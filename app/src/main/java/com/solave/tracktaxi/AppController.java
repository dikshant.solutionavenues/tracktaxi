package com.solave.tracktaxi;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.solave.tracktaxi.preference.TTPreference;
import com.solave.tracktaxi.ui.BaseActivity;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by DELL  374 on 12/27/2016.
 */

public class AppController extends Application {

    private static AppController appController;
    private AQuery aQuery;
    private BaseActivity currentActivity;

    //public static String latitude = null;
    //public static String longitude = null;


    public BaseActivity getActivity() {
        return currentActivity;
    }

    public void setActivity(BaseActivity currentActivity) {
        this.currentActivity = currentActivity;
    }

    public static AppController getInstance() {
        return appController;
    }

    public AQuery getWSInstance() {
        return aQuery;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appController = this;
        TTPreference.init(appController);
        aQuery = new AQuery(this);
        AjaxCallback.setNetworkLimit(8);
    }

    HashMap<String,String> map = new HashMap<String, String>();

    public synchronized void updateLocation()
    {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                if(!TextUtils.isEmpty(TTPreference.getInstance().getStringValue(TTPreference.TAXI_NUMBER))
                        && !TextUtils.isEmpty(TTPreference.getInstance().getStringValue(TTPreference.LATITUDE))
                        && !TextUtils.isEmpty(TTPreference.getInstance().getStringValue(TTPreference.LONGITUDE))
                        )
                map.clear();
                map.put("car_regn_number",TTPreference.getInstance().getStringValue(TTPreference.TAXI_NUMBER));
                map.put("latitude",TTPreference.getInstance().getStringValue(TTPreference.LATITUDE));
                map.put("longitude",TTPreference.getInstance().getStringValue(TTPreference.LONGITUDE));
                aQuery.ajaxCancel();
                aQuery.ajax("http://www.rcstaxi.com/api.php",map, String.class,new AjaxCallback<String>(){
                    @Override
                    public void callback(String url, String data, AjaxStatus status) {
                        super.callback(url, data, status);
                        if (BuildConfig.DEBUG) {
                            Log.d("###$Request URL", url + "");
                            Log.d("###$Request ", map + "");
                            Log.d("###$Response ", data + "");
                            Log.d("###$Status Message : ", status.getMessage() + "");
                            Log.d("###$Status Code : ", status.getCode() + "");
                        }
                    }
                });
            }
        }, 0, 1000 * 10 );//put here time 1000 milliseconds=1 second
    }
}
