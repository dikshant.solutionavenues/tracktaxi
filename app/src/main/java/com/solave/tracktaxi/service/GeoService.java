package com.solave.tracktaxi.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;


import com.solave.tracktaxi.AppController;
import com.solave.tracktaxi.preference.TTPreference;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Hbworld on 8/5/2015.
 */
public class GeoService extends Service {
    GPSTracker gps;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        // Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();
        gps = new GPSTracker(GeoService.this);

        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            //String addressText = getCurrentLocationViaJSON(latitude,longitude);
            TTPreference.getInstance().saveStringValue(TTPreference.LATITUDE,String.valueOf(latitude)); ;
            TTPreference.getInstance().saveStringValue(TTPreference.LONGITUDE,String.valueOf(longitude)); ;
            callReceiver(true);
        } else {
            try {
                gps.showSettingsAlert();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return START_STICKY;
    }

    public void callReceiver(boolean gotLocation) {
        /*Intent intent = new Intent();
        intent.setAction("com.solave.tracktaxi.service.location");
        intent.putExtra("location", gotLocation);
        // LocalBroadcastManager.getInstance(AppController.getInstance().getActivity()).sendBroadcast(intent);
        if (AppController.getInstance().getActivity() != null)
            AppController.getInstance().getActivity().sendBroadcast(intent);*/

        AppController.getInstance().updateLocation();
    }

    public String getCurrentLocationViaJSON(double lat,
                                            double lng) {


        JSONObject jsonObj = getLocationInfo(lat, lng);
        Log.i("JSON string =>", jsonObj.toString());

        String Address1 = "";
        String Address2 = "";
        String City = "";
        String State = "";
        String Country = "";
        String PIN = "";
        String currentLocation = "";

        try {
            String status = jsonObj.getString("status").toString();
            Log.i("status", status);

            if (status.equalsIgnoreCase("OK")) {
                JSONArray Results = jsonObj.getJSONArray("results");
                JSONObject zero = Results.getJSONObject(0);
                JSONArray address_components = zero
                        .getJSONArray("address_components");

                for (int i = 0; i < address_components.length(); i++) {
                    JSONObject zero2 = address_components.getJSONObject(i);
                    String long_name = zero2.getString("long_name");
                    JSONArray mtypes = zero2.getJSONArray("types");
                    String Type = mtypes.getString(0);

                    if (Type.equalsIgnoreCase("sublocality_level_2")) {
                        Address1 = long_name;
                    } else if (Type.equalsIgnoreCase("sublocality_level_1")) {
                        Address2 = long_name;

                    } else if (Type.equalsIgnoreCase("locality")) {
                        City = long_name;

                    }

                }

                if (Address1 != null) {
                    currentLocation = Address1;
                }
                if (Address2 != null) {
                    currentLocation = currentLocation + ", " + Address2;
                }

                currentLocation = currentLocation + ", " + City;
            }
        } catch (Exception e) {

        }
        return currentLocation;

    }

    private JSONObject getLocationInfo(double lat, double lng) {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();

            StrictMode.setThreadPolicy(policy);

            HttpGet httpGet = new HttpGet(
                    "http://maps.googleapis.com/maps/api/geocode/json?latlng="
                            + lat + "," + lng + "&sensor=true");
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            StringBuilder stringBuilder = new StringBuilder();

            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {

            } catch (IOException e) {

            }

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(stringBuilder.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jsonObject;
        }
        return null;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }
}
